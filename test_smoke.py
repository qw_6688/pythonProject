import requests
import allure


@allure.feature("宠物商店宠物信息接口测试")
class testSmoke:
    def setup_class(self):
        self.url = "http://petstore.swagger.io/v2/pet"
        self.params = {
            "id": 0,
            "category": {
                "id": 0,
                "name": "string"
            },
            "name": "doggie",
            "photoUrls": [
                "string"
            ],
            "tags": [
                {
                    "id": 0,
                    "name": "string"
                }
            ],
            "status": "available"
        }
        self.find_params = {
            "status": "available"
        }

    @allure.story("查询宠物接口冒烟用例")
    def test_getpet(self):
        get_url = self.url + "/findByStatus"
        with allure.step("发出查询接口请求"):
            r = requests.get(get_url, self.find_parmas)
        with allure.step("获取查询接口响应"):
            print(r.json())
        with allure.step("查询接口断言"):
            assert r.status_code == 200
