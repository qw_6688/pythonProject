import pytest
from calculate import add

@pytest.mark.hebeu
def test_add():
        print("开始计算")
        A = 2 + 3
        assert A==5
        print("结束计算")

@pytest.mark.hebeu
def test_add2():
    print("开始计算")
    B = 3 + 3.5
    assert B== 6.5
    print("结束计算")

@pytest.mark.hebeu
def test_add3():
    print("开始计算")
    C = 0.5 + 3.5
    assert C == 4
    print("结束计算")

    print("结束测试")